<?php

/**
 * List scorable events provided by your module. This must return an array of strings.
 * Each returned string will appear on the admin interface as an event that can be
 * assigned a point value.
 */
function hook_content_autoscore_list() {
  return array(
    'dummy event',
  );
}

/**
 * @param $nid      - The node we're interested in
 * @param $event    - The event we want to count
 * @return int|null - The number of times $event has happened to node $nid.
 *
 * All events defined in your module's hook_content_autoscore_list are fair game here.
 * If your implementation of this hook is ever called with an $event not defined in your
 * hook_content_autoscore_list(), something is wrong with Content Autoscore. File a bug ;-)
 *
 * Total node scores are cached, so this function will not be called every time the score
 * for a node is requested.
 */
function hook_content_autoscore_count($nid, $event) {
  $count = NULL;
  switch ($event) {
    case 'dummy event':
      //Dummy event never happens, so we just return 0.
      $count = 0;
      break;
  }

  return $count;
}